// eslint-disable-next-line import/no-extraneous-dependencies
const Bluebird = require('bluebird')
const test = require('blue-tape')
// eslint-disable-next-line import/no-extraneous-dependencies

const { config } = require('../lib')

test.onFinish(() => {
  config.messageStore.stop()
  config.db.then(client => client.destroy())
})

/* eslint-disable no-console */
process.on('unhandledRejection', err => {
  console.error('Uh-oh. Unhandled Rejection')
  console.error(err)

  process.exit(1)
})
/* eslint-enable no-console */

module.exports = {
  config
}
