const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a Cataloged', t => {
  // There is no actual video-publishing component in this set up, so we don't
  // have a control to pull in.  We have to hand make it using the event model.
  const videoId = uuid()
  const cataloged = {
    id: uuid(),
    type: 'Cataloged',
    data: {
      videoId,
      ownerId: uuid(),
      catalogedDate: '2020-03-30'
    }
  }
  cataloged.globalPosition = 1

  const { Cataloged } = config.videosAggregator.handlers

  return Cataloged(cataloged)
    .then(() => Cataloged(cataloged))
    .then(() =>
      config.db.then(client =>
        client('videos')
          .where({ video_id: videoId })
          .then(([video]) => {
            t.assert(video, 'Got the video')

            t.equal(video.owner_id, cataloged.data.ownerId, 'Same ownerId')
          })
      )
    )
})
