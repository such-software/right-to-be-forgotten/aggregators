const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a Published', t => {
  // There is no actual video-publishing component in this set up, so we don't
  // have a control to pull in.  We have to hand make it using the event model.
  const videoId = uuid()
  const published = {
    id: uuid(),
    type: 'Published',
    data: {
      videoId,
      publishedDate: '2020-03-31'
    }
  }
  published.globalPosition = 3

  const video = { video_id: videoId, owner_id: uuid(), last_global_position: 2 }

  const { Published } = config.videosAggregator.handlers

  return config.db
    .then(client => client('videos').insert(video))
    .then(() => Published(published))
    .then(() => Published(published))
    .then(() =>
      config.db.then(client =>
        client('videos')
          .where({ video_id: videoId })
          .then(([video]) => {
            t.assert(video, 'Got the video')

            const publishedDate = video.published_date
              .toISOString()
              .split('T')[0]

            t.equal(publishedDate, published.data.publishedDate, 'Same date')
          })
      )
    )
})
