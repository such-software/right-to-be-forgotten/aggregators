const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a Titled', t => {
  // There is no actual video-publishing component in this set up, so we don't
  // have a control to pull in.  We have to hand make it using the event model.
  const videoId = uuid()
  const titled = {
    id: uuid(),
    type: 'Titled',
    data: {
      videoId,
      title: 'Never Gonna Give You Up'
    }
  }
  titled.globalPosition = 2

  const video = { video_id: videoId, owner_id: uuid() }

  const { Titled } = config.videosAggregator.handlers

  return config.db
    .then(client => client('videos').insert(video))
    .then(() => Titled(titled))
    .then(() => Titled(titled))
    .then(() =>
      config.db.then(client =>
        client('videos')
          .where({ video_id: videoId })
          .then(([video]) => {
            t.assert(video, 'Got the video')

            t.equal(video.title, titled.data.title, 'Same title')
          })
      )
    )
})
