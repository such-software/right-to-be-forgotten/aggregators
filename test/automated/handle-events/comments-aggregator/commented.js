const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a comment', t => {
  const commented = {
    id: uuid(),
    type: 'Commented',
    data: {
      commentId: uuid(),
      videoId: uuid(),
      commenterId: uuid(),
      commentTime: '2000-01-01T00:00:00',
      body: 'comment'
    }
  }
  commented.globalPosition = 1

  const commentsHandlers = config.commentsAggregator.handlers

  return commentsHandlers
    .Commented(commented)
    .then(() => commentsHandlers.Commented(commented))
    .then(() =>
      config.db.then(client =>
        client('comments')
          .where({ video_id: commented.data.videoId })
          .then(([comment]) => {
            t.assert(comment, 'Got the comment')

            t.equal(
              comment.comment_id,
              commented.data.commentId,
              'Same comment id'
            )
            t.equal(
              comment.commenter_id,
              commented.data.commenterId,
              'Same commenter id'
            )

            const actualTime = comment.comment_time.getTime()
            const expectedTime = new Date(commented.data.commentTime).getTime()

            t.equal(actualTime, expectedTime, 'Same time')
            t.equal(comment.body, commented.data.body, 'Same body')
          })
      )
    )
})
