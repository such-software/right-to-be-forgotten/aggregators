const registrationComponent = require('@suchsoftware/gdpr-registration-component')
const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a Named', t => {
  const named = registrationComponent.controls.events.named.example()
  const { userId } = named.data

  const user = {
    user_id: userId,
    email: 'ethan@suchsoftware.com',
    registered_date: new Date(),
    last_global_position: 2
  }

  const otherUser = {
    user_id: uuid(),
    email: 'leavealone@example.com',
    registered_date: new Date(),
    last_global_position: 1
  }

  named.globalPosition = user.last_global_position + 1

  const { Named } = config.usersAggregator.handlers

  return config.db
    .then(client =>
      client('users')
        .insert(user)
        .then(() => client('users').insert(otherUser))
    )
    .then(() => Named(named))
    .then(() => Named(named))
    .then(() =>
      config.db.then(client =>
        client('users').then(users => {
          const change = users.find(u => u.user_id === userId)
          const leave = users.find(u => u.user_id === otherUser.user_id)

          t.assert(change, 'Got the user')
          t.assert(leave, 'Got the other user')

          t.equal(change.name, 'Ethan Garofolo', 'Name is added')
          t.assert(!leave.name, 'Other user not named')
        })
      )
    )
})
