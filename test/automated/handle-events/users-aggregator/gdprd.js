const registrationComponent = require('@suchsoftware/gdpr-registration-component')
const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a Gdprd', t => {
  const gdprd = registrationComponent.controls.events.gdprd.example()
  const { userId } = gdprd.data

  const user = {
    user_id: userId,
    email: 'ethan@suchsoftware.com',
    name: 'Ethan Garofolo',
    registered_date: new Date(),
    last_global_position: 2
  }

  const otherUser = {
    user_id: uuid(),
    email: 'leavealone@example.com',
    registered_date: new Date(),
    last_global_position: 1
  }

  gdprd.globalPosition = user.last_global_position + 1

  const { Gdprd } = config.usersAggregator.handlers

  return config.db
    .then(client =>
      client('users')
        .insert(user)
        .then(() => client('users').insert(otherUser))
    )
    .then(() => Gdprd(gdprd))
    .then(() => Gdprd(gdprd))
    .then(() =>
      config.db.then(client =>
        client('users').then(users => {
          const change = users.find(u => u.user_id === userId)
          const leave = users.find(u => u.user_id === otherUser.user_id)

          t.assert(change, 'Got the user')
          t.assert(leave, 'Got the other user')

          t.equal(change.email, "==GDPR'd==", 'Email is forgotten')
          t.equal(change.name, "==GDPR'd==", 'Name is forgotten')
          t.equal(leave.email, 'leavealone@example.com', 'Email is forgotten')
          t.assert(!leave.name, 'Name is not changed')
        })
      )
    )
})
