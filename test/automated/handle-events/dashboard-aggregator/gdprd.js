const registrationComponent = require('@suchsoftware/gdpr-registration-component')
const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const { config } = require('../../../automated-init')

test('It aggregates a Gdprd', t => {
  const gdprd = registrationComponent.controls.events.gdprd.example()
  const { userId } = gdprd.data

  const user = {
    user_id: userId,
    last_global_position: 2
  }

  const otherUser = {
    user_id: uuid(),
    last_global_position: 1
  }

  gdprd.globalPosition = user.last_global_position + 1

  const { Gdprd } = config.dashboardAggregator.handlers

  return config.db
    .then(client =>
      client('dashboard')
        .insert(user)
        .then(() => client('dashboard').insert(otherUser))
    )
    .then(() => Gdprd(gdprd))
    .then(() => Gdprd(gdprd))
    .then(() =>
      config.db.then(client =>
        client('dashboard').then(users => {
          const change = users.find(u => u.user_id === userId)
          const leave = users.find(u => u.user_id === otherUser.user_id)

          t.assert(change, 'Got the user')
          t.assert(leave, 'Got the other user')

          t.equal(change.name, "==GDPR'd==", 'Name is forgotten')
          t.assert(change.forgotten_at, 'Forgotten')
          t.assert(!leave.name, 'Name is not changed')
          t.assert(!leave.forgotten_at, 'Not forgotten')
        })
      )
    )
})
