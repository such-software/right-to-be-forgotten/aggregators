const registrationComponent = require('@suchsoftware/gdpr-registration-component')
const test = require('blue-tape')

const { config } = require('../../../automated-init')

test('It aggregates a registration', t => {
  const registered = registrationComponent.controls.events.registered.example()
  registered.globalPosition = 1

  const { Registered } = config.dashboardAggregator.handlers

  return Registered(registered)
    .then(() => Registered(registered))
    .then(() =>
      config.db.then(client =>
        client('dashboard')
          .where({ user_id: registered.data.userId })
          .then(([user]) => {
            t.assert(user, 'Got the user')
          })
      )
    )
})
