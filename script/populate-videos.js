const Bluebird = require('bluebird')
const { v4: uuid } = require('uuid')

const createConfig = require('../lib/config')
const env = require('../lib/env')

const config = createConfig({ env })

const videoIds = [uuid(), uuid(), uuid()]
const ownerIds = [uuid(), uuid(), uuid()]

const video1Events = [
  {
    id: uuid(),
    type: 'Cataloged',
    data: {
      videoId: videoIds[0],
      ownerId: ownerIds[0],
      catalogedDate: '2020-01-01'
    }
  },
  {
    id: uuid(),
    type: 'Titled',
    data: {
      videoId: videoIds[0],
      title: 'Video 1'
    }
  },
  {
    id: uuid(),
    type: 'Published',
    data: {
      videoId: videoIds[0],
      publishedDate: '2020-01-02'
    }
  }
]
const video1StreamName = `videoPublishing-${videoIds[0]}`

const video2Events = [
  {
    id: uuid(),
    type: 'Cataloged',
    data: {
      videoId: videoIds[1],
      ownerId: ownerIds[1],
      catalogedDate: '2020-02-01'
    }
  },
  {
    id: uuid(),
    type: 'Titled',
    data: {
      videoId: videoIds[1],
      title: 'Video 2'
    }
  },
  {
    id: uuid(),
    type: 'Published',
    data: {
      videoId: videoIds[1],
      publishedDate: '2020-02-02'
    }
  }
]
const video2StreamName = `videoPublishing-${videoIds[1]}`

const video3Events = [
  {
    id: uuid(),
    type: 'Cataloged',
    data: {
      videoId: videoIds[2],
      ownerId: ownerIds[2],
      catalogedDate: '2020-03-01'
    }
  },
  {
    id: uuid(),
    type: 'Titled',
    data: {
      videoId: videoIds[2],
      title: 'Video 3'
    }
  },
  {
    id: uuid(),
    type: 'Published',
    data: {
      videoId: videoIds[2],
      publishedDate: '2020-03-02'
    }
  }
]
const video3StreamName = `videoPublishing-${videoIds[2]}`

Bluebird.each(video1Events, event =>
  config.messageStore.write(video1StreamName, event)
)
  .then(() =>
    Bluebird.each(video2Events, event =>
      config.messageStore.write(video2StreamName, event)
    )
  )
  .then(() =>
    Bluebird.each(video3Events, event =>
      config.messageStore.write(video3StreamName, event)
    )
  )
  .finally(() => {
    config.messageStore.stop()
    config.db.then(client => client.destroy())
  })
