function pad (num) {
  return num < 10 ? `0${num}` : `${num}`
}

module.exports = {
  iso8601 () {
    return new Date().toISOString()
  },

  iso8601Date () {
    const time = new Date()

    const year = pad(time.getUTCFullYear())
    const month = pad(time.getUTCMonth() + 1)
    const day = pad(time.getUTCDate())

    return `${year}-${month}-${day}`
  }
}
