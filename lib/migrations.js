const commentsMigration = require('./comments-aggregator/migration')
const dashboardMigration = require('./dashboard-aggregator/migration')
const usersMigration = require('./users-aggregator/migration')
const videosMigration = require('./videos-aggregator/migration')

module.exports = {
  tableName: 'aggregator_migrations',

  getMigrations () {
    return Promise.resolve(['dashboard', 'users', 'videos', 'comments'])
  },

  getMigrationName (migration) {
    return migration
  },

  getMigration (migration) {
    switch (migration) {
      case 'comments':
        return commentsMigration
      case 'dashboard':
        return dashboardMigration
      case 'users':
        return usersMigration
      case 'videos':
        return videosMigration
      default:
        return null
    }
  }
}
