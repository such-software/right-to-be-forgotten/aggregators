const UpsertComment = require('./queries/upsert-comment')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const upsertComment = UpsertComment({ db })
  const handlers = Handlers({ upsertComment })

  const subscription = messageStore.createSubscription({
    streamName: 'videoComment',
    handlers,
    subscriberId: 'videoCommentsAggregator'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
