function build ({ upsertComment }) {
  return {
    async Commented (commented) {
      const commentTime = new Date(commented.data.commentTime)

      return upsertComment(
        commented.data.commentId,
        commented.data.videoId,
        commented.data.commenterId,
        commentTime,
        commented.data.body,
        commented.globalPosition
      )
    }
  }
}

module.exports = build
