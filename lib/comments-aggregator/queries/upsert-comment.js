function build ({ db }) {
  return function upsert (
    commentId,
    videoId,
    commenterId,
    commentTime,
    body,
    globalPosition
  ) {
    const rawQuery = `
      INSERT INTO 
        comments (comment_id, video_id, commenter_id, comment_time, body, last_global_position)
      VALUES
        (:commentId, :videoId, :commenterId, :commentTime, :body, :globalPosition)
      ON CONFLICT DO NOTHING
    `

    return db.then(client =>
      client.raw(rawQuery, {
        commentId,
        videoId,
        commenterId,
        commentTime,
        body,
        globalPosition
      })
    )
  }
}

module.exports = build
