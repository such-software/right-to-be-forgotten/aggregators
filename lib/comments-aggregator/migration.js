module.exports = {
  up (knex) {
    return knex.schema.createTable('comments', table => {
      table.string('comment_id').primary()
      table.string('video_id')
      table.string('commenter_id')
      table.datetime('comment_time')
      table.string('body')
      table.bigInteger('last_global_position').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('comments')
  }
}
