const Bluebird = require('bluebird')
const knex = require('knex')

const migrations = require('./migrations')

function createKnexClient ({ connectionString }) {
  const client = knex(connectionString)

  const migrationSettings = {
    tableName: 'aggregator_migrations',
    migrationSource: migrations
  }

  // Wrap in Bluebird.resolve to guarantee a Bluebird Promise down the chain
  return Bluebird.resolve(client.migrate.latest(migrationSettings))
    .then(() => client)
}

module.exports = createKnexClient
