const ForgetUser = require('./queries/forget-user')
const NameUser = require('./queries/name-user')
const UpsertUser = require('./queries/upsert-user')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const forgetUser = ForgetUser({ db })
  const nameUser = NameUser({ db })
  const upsertUser = UpsertUser({ db })
  const handlers = Handlers({ forgetUser, nameUser, upsertUser })

  const subscription = messageStore.createSubscription({
    streamName: 'registration',
    handlers,
    subscriberId: 'dashboardAggregatorRegistration'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
