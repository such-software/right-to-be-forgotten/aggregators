function build ({ db }) {
  return function upsert (userId, forgottenAt, globalPosition) {
    return db.then(client =>
      client('dashboard')
        .update({ name: "==GDPR'd==", forgotten_at: forgottenAt })
        .where({ user_id: userId })
        .where('last_global_position', '<', globalPosition)
    )
  }
}

module.exports = build
