function build ({ db }) {
  return function upsert (userId, globalPosition) {
    const rawQuery = `
      INSERT INTO 
        dashboard (user_id, last_global_position)
      VALUES
        (:userId, :globalPosition)
      ON CONFLICT DO NOTHING
    `

    return db.then(client => client.raw(rawQuery, { userId, globalPosition }))
  }
}

module.exports = build
