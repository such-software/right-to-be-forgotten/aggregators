module.exports = {
  up (knex) {
    return knex.schema.createTable('dashboard', table => {
      table.string('user_id').primary()
      table.datetime('forgotten_at')
      table.string('name')
      table.bigInteger('last_global_position').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('dashboard')
  }
}
