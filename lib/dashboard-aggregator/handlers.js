function build ({ forgetUser, nameUser, upsertUser }) {
  return {
    async Registered (registered) {
      const registeredDate = new Date(registered.data.registeredDate)

      return upsertUser(registered.data.userId, registered.globalPosition)
    },

    async Named (named) {
      return nameUser(named.data.userId, named.data.name, named.globalPosition)
    },

    async Gdprd (gdprd) {
      const forgottenAt = new Date(gdprd.data.effectiveTime)

      return forgetUser(gdprd.data.userId, forgottenAt, gdprd.globalPosition)
    }
  }
}

module.exports = build
