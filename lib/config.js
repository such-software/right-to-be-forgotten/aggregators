/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const MessageStore = require('@suchsoftware/proof-of-concept-message-store')

const KnexClient = require('./knex-client')
const PostgresClient = require('./postgres-client')

const clock = require('./clock')
const UsersAggregator = require('./users-aggregator')
const DashboardAggregator = require('./dashboard-aggregator')
const VideosAggregator = require('./videos-aggregator')
const CommentsAggregator = require('./comments-aggregator')

function createConfig ({ env }) {
  const knexClient = KnexClient({
    connectionString: env.databaseUrl
  })
  const postgresClient = PostgresClient({
    connectionString: env.messageStoreConnectionString
  })
  const messageStore = MessageStore({ session: postgresClient })
  const usersAggregator = UsersAggregator({ db: knexClient, messageStore })
  const dashboardAggregator = DashboardAggregator({
    db: knexClient,
    messageStore
  })
  const videosAggregator = VideosAggregator({ db: knexClient, messageStore })
  const commentsAggregator = CommentsAggregator({
    db: knexClient,
    messageStore
  })

  const components = [
    usersAggregator,
    dashboardAggregator,
    videosAggregator,
    commentsAggregator
  ]

  return {
    components,
    env,
    db: knexClient,
    messageStore,
    usersAggregator,
    dashboardAggregator,
    videosAggregator,
    commentsAggregator
  }
}

module.exports = createConfig
