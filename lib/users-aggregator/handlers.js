function build ({ forgetUser, nameUser, upsertUser }) {
  return {
    async Registered (registered) {
      const registeredDate = new Date(registered.data.registeredDate)

      return upsertUser(
        registered.data.userId,
        registered.data.email,
        registeredDate,
        registered.globalPosition
      )
    },

    async Named (named) {
      return nameUser(named.data.userId, named.data.name, named.globalPosition)
    },

    async Gdprd (gdprd) {
      return forgetUser(gdprd.data.userId, gdprd.globalPosition)
    }
  }
}

module.exports = build
