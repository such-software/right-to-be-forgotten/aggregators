module.exports = {
  up (knex) {
    return knex.schema.createTable('users', table => {
      table.string('user_id').primary()
      table.string('email')
      table.string('name')
      table.datetime('registered_date')
      table.bigInteger('last_global_position').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('users')
  }
}
