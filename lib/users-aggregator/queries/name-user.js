function build ({ db }) {
  return function nameUser (userId, name, globalPosition) {
    return db.then(client =>
      client('users')
        .update({ name })
        .where({ user_id: userId })
        .where('last_global_position', '<', globalPosition)
    )
  }
}

module.exports = build
