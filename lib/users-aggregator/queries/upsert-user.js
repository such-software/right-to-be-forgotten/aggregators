function build ({ db }) {
  return function upsert (userId, email, registeredDate, globalPosition) {
    const rawQuery = `
      INSERT INTO 
        users (user_id, email, registered_date, last_global_position)
      VALUES
        (:userId, :email, :registeredDate, :globalPosition)
      ON CONFLICT DO NOTHING
    `

    return db.then(client =>
      client.raw(rawQuery, { userId, email, registeredDate, globalPosition })
    )
  }
}

module.exports = build
