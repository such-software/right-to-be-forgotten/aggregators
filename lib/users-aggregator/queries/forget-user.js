function build ({ db }) {
  return function upsert (userId, globalPosition) {
    return db.then(client =>
      client('users')
        .update({ email: "==GDPR'd==", name: "==GDPR'd==" })
        .where({ user_id: userId })
        .where('last_global_position', '<', globalPosition)
    )
  }
}

module.exports = build
