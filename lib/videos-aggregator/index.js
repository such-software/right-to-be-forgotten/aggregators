const PublishVideo = require('./queries/publish-video')
const TitleVideo = require('./queries/title-video')
const UpsertVideo = require('./queries/upsert-video')
const Handlers = require('./handlers')

function build ({ db, messageStore }) {
  const publishVideo = PublishVideo({ db })
  const titleVideo = TitleVideo({ db })
  const upsertVideo = UpsertVideo({ db })
  const handlers = Handlers({ publishVideo, titleVideo, upsertVideo })

  const subscription = messageStore.createSubscription({
    streamName: 'videoPublishing',
    handlers,
    subscriberId: 'videosAggregatorVideoPublishing'
  })

  function start () {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
