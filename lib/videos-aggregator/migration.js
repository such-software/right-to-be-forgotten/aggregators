module.exports = {
  up (knex) {
    return knex.schema.createTable('videos', table => {
      table.string('video_id').primary()
      table.string('owner_id')
      table.string('title')
      table.datetime('published_date')
      table.bigInteger('last_global_position').defaultTo(0)
    })
  },

  down (knex) {
    return knex.schema.dropTable('videos')
  }
}
