function build ({ db }) {
  return function titleVideo (videoId, title, globalPosition) {
    return db.then(client =>
      client('videos')
        .update({ title })
        .where({ video_id: videoId })
        .where('last_global_position', '<', globalPosition)
    )
  }
}

module.exports = build
