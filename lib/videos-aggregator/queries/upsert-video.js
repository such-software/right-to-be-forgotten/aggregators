function build ({ db }) {
  return function upsert (videoId, ownerId, globalPosition) {
    const rawQuery = `
      INSERT INTO 
        videos (video_id, owner_id, last_global_position)
      VALUES
        (:videoId, :ownerId, :globalPosition)
      ON CONFLICT DO NOTHING
    `

    return db.then(client =>
      client.raw(rawQuery, { videoId, ownerId, globalPosition })
    )
  }
}

module.exports = build
