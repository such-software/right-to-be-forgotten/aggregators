function build ({ db }) {
  return function publishVideo (videoId, publishedDate, globalPosition) {
    return db.then(client =>
      client('videos')
        .update({ published_date: publishedDate })
        .where({ video_id: videoId })
        .where('last_global_position', '<', globalPosition)
    )
  }
}

module.exports = build
