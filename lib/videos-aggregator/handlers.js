function build ({ publishVideo, titleVideo, upsertVideo }) {
  return {
    async Cataloged (cataloged) {
      return upsertVideo(
        cataloged.data.videoId,
        cataloged.data.ownerId,
        cataloged.globalPosition
      )
    },

    async Titled (titled) {
      return titleVideo(
        titled.data.videoId,
        titled.data.title,
        titled.globalPosition
      )
    },

    async Published (published) {
      const publishedDate = new Date(published.data.publishedDate)

      return publishVideo(
        published.data.videoId,
        publishedDate,
        published.globalPosition
      )
    }
  }
}

module.exports = build
